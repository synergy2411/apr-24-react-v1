import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  getAuth,
  signOut,
} from "firebase/auth";
import { initializeApp } from "firebase/app";
// User Registration

const app = initializeApp({
  apiKey: "AIzaSyDBUIbWLPX4ZWiFFUp84z_Ss8nmxMYNszI",
  authDomain: "apr-24-react.firebaseapp.com",
});

const auth = getAuth(app);

export const authRegistration = createAsyncThunk(
  "auth/registration",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const userCredentials = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      const token = userCredentials.user.getIdToken();
      return token;
    } catch (err) {
      return rejectWithValue("Unable to create User");
    }
  }
);

export const authLogin = createAsyncThunk(
  "user/login",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const userCredentials = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      return userCredentials.user.getIdToken();
    } catch (err) {
      console.log(err);
      return rejectWithValue("Unable to login");
    }
  }
);

export const authLogout = createAsyncThunk(
  "user/logout",
  async (_, { rejectWithValue }) => {
    try {
      await signOut(auth);
    } catch (Err) {
      return rejectWithValue("Unable to logout");
    }
  }
);

const initialState = {
  token: null,
  loading: false,
  error: null,
};

const authSlice = createSlice({
  name: "user-registration",
  initialState,
  extraReducers: (builder) => {
    //    User Registration
    builder.addCase(authRegistration.pending, (state, action) => {
      state.loading = true;
      state.token = null;
    });
    builder.addCase(authRegistration.fulfilled, (state, action) => {
      state.token = action.payload;
      state.loading = false;
      state.error = null;
    });
    builder.addCase(authRegistration.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.token = null;
    });
    //   User Login
    builder.addCase(authLogin.pending, (state, action) => {
      state.loading = true;
      state.token = null;
    });
    builder.addCase(authLogin.fulfilled, (state, action) => {
      state.token = action.payload;
      state.loading = false;
      state.error = null;
    });
    builder.addCase(authLogin.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.token = null;
    });
    //   User Logout
    builder.addCase(authLogout.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(authLogout.fulfilled, (state, action) => {
      state.token = null;
      state.loading = false;
      state.error = null;
    });
    builder.addCase(authLogout.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.token = null;
    });
  },
});

const authReducer = authSlice.reducer;
export default authReducer;
