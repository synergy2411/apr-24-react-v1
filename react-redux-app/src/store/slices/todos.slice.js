import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todos: [
    { id: "t001", label: "buy grocery", status: "pending" },
    { id: "t002", label: "pot the plants", status: "completed" },
    { id: "t003", label: "renew car insurance", status: "pending" },
    { id: "t004", label: "buy new jeans", status: "completed" },
  ],
};

const todoSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.todos.push(action.payload);
    },
    deleteTodo: (state, action) => {
      const position = state.todos.findIndex(
        (todo) => todo.id === action.payload
      );
      if (position === -1) {
        throw new Error("Unable to delete the item for " + action.payload);
      }
      state.todos.splice(position, 1);
    },
  },
});

// Actions
export const { addTodo, deleteTodo } = todoSlice.actions;

// Reducer
const todoReducer = todoSlice.reducer;
export default todoReducer;
