import { configureStore } from "@reduxjs/toolkit";
import todoReducer from "./slices/todos.slice";
import authReducer from "./slices/auth.slice";

// STORE
const store = configureStore({
  reducer: {
    todoReducer,
    authReducer,
  },
});

export default store;
