import { useSelector } from "react-redux";
import Auth from "./Components/Auth/Auth";
import Todos from "./Components/Todos/Todos";

function App() {
  const { token } = useSelector((store) => store.authReducer);
  return (
    <div className="container">
      {token && <Todos />}

      {!token && <Auth />}
    </div>
  );
}

export default App;
