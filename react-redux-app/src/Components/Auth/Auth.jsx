import { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { authRegistration, authLogin } from "../../store/slices/auth.slice";

function Auth() {
  const emailRef = useRef();
  const passwordRef = useRef();
  const dispatch = useDispatch();
  const { token, loading, error } = useSelector((store) => store.authReducer);

  const submitHandler = (event) => {
    event.preventDefault();
    let user = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
    };

    dispatch(authRegistration(user));
  };

  const loginClickHandler = () => {
    let user = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
    };
    dispatch(authLogin(user));
  };

  return (
    <div className="row">
      <div className="col-8 offset-2">
        {error && <p className="alert alert-danger">{error}</p>}
        {token && <p className="alert alert-success">User created</p>}
        <h1 className="text-center">Auth Form</h1>
        <form onSubmit={submitHandler}>
          {/* email */}
          <div className="form-floating mb-3">
            <input
              type="email"
              className="form-control"
              name="email"
              id="email"
              placeholder=""
              ref={emailRef}
            />
            <label htmlFor="email">Email</label>
          </div>

          {/* password */}
          <div className="form-floating mb-3">
            <input
              type="password"
              className="form-control"
              name="password"
              id="password"
              placeholder=""
              ref={passwordRef}
            />
            <label htmlFor="password">Password</label>
          </div>
          {/* buttons */}
          <div className="row">
            <div className="col-6">
              <div className="d-grid">
                <button
                  className="btn btn-success"
                  type="submit"
                  disabled={loading}
                >
                  {loading ? "Submitting..." : "Sign up"}
                </button>
              </div>
            </div>
            <div className="col-6">
              <div className="d-grid">
                <button className="btn btn-primary" onClick={loginClickHandler}>
                  Login
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Auth;
