import classes from "./TodoItem.module.css";
import { useDispatch } from "react-redux";
import { deleteTodo } from "../../store/slices/todos.slice";

function TodoItem({ todo }) {
  const dispatch = useDispatch();

  const itemClickHandler = () => {
    dispatch(deleteTodo(todo.id));
  };

  return (
    <li
      onClick={itemClickHandler}
      className={`list-group-item ${classes["clickable"]}`}
      key={todo.id}
    >
      {todo.label.toUpperCase()}
    </li>
  );
}

export default TodoItem;
