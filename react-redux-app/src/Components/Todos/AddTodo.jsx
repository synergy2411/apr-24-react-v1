import { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../../store/slices/todos.slice";

function AddTodo() {
  const [enteredLabel, setEnteredLabel] = useState("");

  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    let newTodo = {
      id: "t0" + Math.round(Math.random() * 100),
      label: enteredLabel,
      status: "pending",
    };
    dispatch(addTodo(newTodo));
    setEnteredLabel("");
  };
  return (
    <>
      <form onSubmit={submitHandler}>
        <div className="row">
          <div className="col-9">
            {/* label : Controlled Component */}
            <input
              type="text"
              className="form-control"
              name="label"
              value={enteredLabel}
              onChange={(e) => setEnteredLabel(e.target.value)}
            />
          </div>
          <div className="col-3">
            {/* button */}
            <div className="d-grid">
              <button type="submit" className="btn btn-primary">
                Add
              </button>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}

export default AddTodo;
