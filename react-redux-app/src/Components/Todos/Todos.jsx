import { useSelector, useDispatch } from "react-redux";
import { authLogout } from "../../store/slices/auth.slice";
import TodoItem from "./TodoItem";
import AddTodo from "./AddTodo";

function Todos() {
  const { todos } = useSelector((store) => store.todoReducer);
  const dispatch = useDispatch();

  return (
    <>
      <button
        className="btn btn-outline-danger"
        onClick={() => dispatch(authLogout())}
      >
        Logout
      </button>
      <h1 className="text-center mb-4">My Todos List</h1>

      <AddTodo />
      <div className="row mt-4">
        <div className="col-10 offset-1">
          <ul className="list-group">
            {todos.map((todo) => (
              <TodoItem todo={todo} key={todo.id} />
            ))}
          </ul>
        </div>
      </div>
    </>
  );
}

export default Todos;
