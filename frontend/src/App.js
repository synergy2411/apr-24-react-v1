import Expenses from "./Components/Expenses/Expenses";
import CallbackDemo from "./Components/Playground/CallbackAndMemoDemo/CallBackDemo";
import ClassBasedCompDemo from "./Components/Playground/ClassBasedCompDemo";
import CompA from "./Components/Playground/ContextDemo/CompA";
import RefAuthCompDemo from "./Components/Playground/RefAuthCompDemo";
import UseEffectDemo from "./Components/Playground/UseEffectDemo";
import UseReducerDemo from "./Components/Playground/UseReducerDemo";

function App() {
  return (
    <div className="container">
      <h1>App works!</h1>
      {/* <Expenses /> */}
      {/* <ClassBasedCompDemo /> */}
      {/* <RefAuthCompDemo /> */}
      {/* <UseEffectDemo /> */}

      {/* <CompA /> */}

      {/* <UseReducerDemo /> */}
      <CallbackDemo />
    </div>
  );
}

export default App;
