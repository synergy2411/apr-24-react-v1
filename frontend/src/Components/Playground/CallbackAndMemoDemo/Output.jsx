import { memo } from "react";

function Output({ toggle }) {
  console.log("Output Renders");
  return (
    <>
      <h3>Output Demo Component</h3>
      {toggle && <p>This paragraphql will appear dynamically</p>}
    </>
  );
}

export default memo(Output);

// prevProps vs currProps
// prevProps === currProps => do not re-render the child component
// prevClickHandler vs currClickHandler
//  - same : memo will not re-render
// - not same : memo wil re-render the child component
