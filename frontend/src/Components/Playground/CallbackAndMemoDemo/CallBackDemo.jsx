import { useState, useCallback, useMemo } from "react";
import Output from "./Output";

function CallbackDemo() {
  const [toggle, setToggle] = useState(false);

  console.log("Callback Demo Renders.");

  //   const clickHandler = useCallback(() => console.log("Clicked"), []); // xixix001
  const clickHandler = useMemo(() => () => console.log("Clicked"), []); // xixix001

  const numbers = useMemo(() => [98, 97, 94, 89, 90], []);

  return (
    <div className="text-center">
      <h1>Callback And Memo Demo Component</h1>

      <button className="btn btn-primary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>
      <hr />

      <Output toggle={true} clickHandler={clickHandler} numbers={numbers} />
    </div>
  );
}

export default CallbackDemo;
