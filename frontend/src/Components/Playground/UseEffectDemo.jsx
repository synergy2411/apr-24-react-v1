import { useEffect, useState } from "react";

function UseEffectDemo() {
  const [toggle, setToggle] = useState(false);
  const [counter, setCounter] = useState(0);

  const [searchTerm, setSearchTerm] = useState("");
  const [repos, setRepos] = useState([]);

  //   useEffect(() => {
  //     console.log("Effect Works");
  //   }, []);

  //   useEffect(() => {
  //     console.log("Effect Works");
  //   }, [counter, toggle]);

  //   useEffect(() => {
  //     console.log("Effect Works");
  //     return () => {
  //       console.log("Clean Up");
  //     };
  //   }, [toggle]);

  useEffect(() => {
    let notifier = setTimeout(() => {
      if (searchTerm.trim() !== "") {
        const fetchRepos = async () => {
          const response = await fetch(
            `https://api.github.com/users/${searchTerm}/repos`
          );
          const repos = await response.json();
          console.log("REPOS  : ", repos);
          setRepos(repos);
        };

        fetchRepos();
      }
    }, 2000);

    return () => {
      clearTimeout(notifier);
    };
  }, [searchTerm]);

  return (
    <>
      <h1>Use Effect Demo Component</h1>
      <button className="btn btn-primary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>

      <button
        className="btn btn-secondary"
        onClick={() => setCounter(counter + 1)}
      >
        {counter}
      </button>
      {toggle && <p>Dynamic paragraph appeared.</p>}

      <hr />

      <input
        type="text"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <br />
      <ul>
        {repos && repos.map((repo) => <li key={repo.id}>{repo.name}</li>)}
      </ul>
    </>
  );
}

export default UseEffectDemo;
