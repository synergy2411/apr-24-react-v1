import { Component } from "react";

class ClassBasedCompDemo extends Component {
  constructor() {
    super();
    this.state = {
      toggle: false,
      counter: 0,
      result: [],
      todos: [],
    };
    console.log("Constructor");
  }

  componentDidMount() {
    console.log("ComponentDidMount");

    const fetchTodos = async () => {
      try {
        const response = await fetch(
          "https://jsonplaceholder.typicode.com/todos"
        );
        const todos = await response.json();
        this.setState({ todos });
      } catch (err) {
        console.error(err);
      }
    };

    fetchTodos();
  }

  componentDidUpdate() {
    console.log("ComponentDidUpdate");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("ShouldComponentUpdate", nextProps, nextState);
    return true;
  }

  toggleHandler = () => this.setState({ toggle: !this.state.toggle });

  increaseCounter = () => this.setState({ counter: this.state.counter + 1 });

  render() {
    console.log("Render");
    return (
      <div>
        <h1>Class based component loaded</h1>
        <button className="btn btn-primary" onClick={this.toggleHandler}>
          Toggle
        </button>
        <button className="btn btn-secondary" onClick={this.increaseCounter}>
          {this.state.counter}
        </button>
        {this.state.toggle && <p>This paragraph is appeared dynamically</p>}
        <hr />
        <ul>
          {this.state.todos.map((todo) => (
            <li key={todo.id}>{todo.title}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ClassBasedCompDemo;
