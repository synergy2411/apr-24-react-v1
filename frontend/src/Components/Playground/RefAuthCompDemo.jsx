import { useRef, useState } from "react";
import { useForm } from "react-hook-form";

function RefAuthCompDemo() {
  const [enteredUsername, setEnteredUsername] = useState("");
  const passwordInputRef = useRef();

  const {
    handleSubmit,
    register,
    formState: { errors, isValid },
  } = useForm();

  const submitHandler = (values) => {
    // e.preventDefault();
    console.log("Values : ", values);
    console.log("Username : ", enteredUsername);
    console.log("Password : ", passwordInputRef.current.value);
  };

  const usernameBlurHandler = () => {
    if (enteredUsername.trim() === "") {
      alert("Username is mandatory field");
      return;
    }

    if (enteredUsername.length < 8) {
      alert("Username should have atleast 8 characters");
      return;
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(submitHandler)}>
        {/* email */}
        <div className="form-floating mb-3">
          <input
            type="text"
            className="form-control"
            name="email"
            id="email"
            placeholder=""
            {...register("email", { required: true, minLength: 6 })}
          />
          <label htmlFor="email">Email</label>
        </div>

        {errors.email && <p>Something went wrong with email</p>}

        {/* username - Controlled Component */}
        <div className="form-floating mb-3">
          <input
            type="text"
            className="form-control"
            name="username"
            id="username"
            placeholder=""
            value={enteredUsername}
            onChange={(e) => setEnteredUsername(e.target.value)}
            onBlur={usernameBlurHandler}
          />
          <label htmlFor="username">Username</label>
        </div>

        {/* password */}
        <div className="form-floating mb-3">
          <input
            type="password"
            className="form-control"
            name="password"
            id="password"
            placeholder=""
            ref={passwordInputRef}
          />
          <label htmlFor="password">Password</label>
        </div>

        <p>Form State Validity: {isValid ? "TRUE" : "FALSE"} </p>

        {/* button */}
        <button className="btn btn-primary" type="submit" disabled={!isValid}>
          Login
        </button>
      </form>
    </div>
  );
}

export default RefAuthCompDemo;
