import { useState } from "react";
import CompB from "./CompB";
import AuthContext from "../../../context/auth-context";

function CompA() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  return (
    <>
      <h1> Component A</h1>

      <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
        <CompB />
      </AuthContext.Provider>
    </>
  );
}

export default CompA;
