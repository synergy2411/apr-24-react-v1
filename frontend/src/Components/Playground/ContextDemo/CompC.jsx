import { useContext } from "react";
import AuthContext from "../../../context/auth-context";

function CompC() {
  const context = useContext(AuthContext);

  return (
    <>
      <h1>Component C</h1>
      {context.isLoggedIn && "CompC - User Is Logged-in"}
      <button
        className="btn btn-secondary"
        onClick={() => context.setIsLoggedIn(!context.isLoggedIn)}
      >
        {context.isLoggedIn ? "Logout" : "Login"}
      </button>
    </>
  );
}

export default CompC;
