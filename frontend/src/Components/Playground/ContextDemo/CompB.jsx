import CompC from "./CompC";
import AuthContext from "../../../context/auth-context";

function CompB() {
  return (
    <AuthContext.Consumer>
      {(context) => {
        return (
          <>
            <h1>Component B</h1>
            <h2>The User is {context.isLoggedIn ? "" : "NOT"} logged-in.</h2>
            <CompC />
          </>
        );
      }}
    </AuthContext.Consumer>
  );
}

export default CompB;
