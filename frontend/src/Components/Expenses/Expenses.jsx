import { useState } from "react";

import ExpenseItem from "./ExpenseItem/ExpenseItem";
import ExpenseForm from "./ExpenseForm/ExpenseForm";
import ExpenseFilter from "./ExpenseFilter/ExpenseFilter";

let INITIAL_EXPENSES = [
  {
    id: "e001",
    title: "Shopping",
    amount: 12.9,
    createdAt: new Date("Dec 21, 2023"),
  },
  {
    id: "e002",
    title: "Planting",
    amount: 29.9,
    createdAt: new Date("Mar 29, 2024"),
  },
  {
    id: "e003",
    title: "Grocery",
    amount: 79.9,
    createdAt: new Date("Oct 12, 2023"),
  },
];

function Expenses() {
  const [isShow, setIsShow] = useState(false);
  const [expenses, setExpenses] = useState(INITIAL_EXPENSES);
  const [selYear, setSelYear] = useState("");

  const addExpenseClickHandler = () => {
    //   isShow = true;            // BAD PRACTICE; NEVER EVER CHANGE THE STATE MUTABLY
    setIsShow(!isShow);
  };

  const addNewExpenseHandler = (newExpense) => {
    // expenses = [newExpense, ...expenses];         // NEVER CHANGE STATE IMMUTABLY
    setExpenses((prevExpenses) => [newExpense, ...prevExpenses]);
    setIsShow(false);
  };

  const deleteExpenseHandler = (expenseId) =>
    setExpenses((prevExpenses) =>
      prevExpenses.filter((exp) => exp.id !== expenseId)
    );

  const closeFormHandler = () => setIsShow(false);

  const filterExpenseHandler = (selectedYear) => setSelYear(selectedYear);

  let filteredExpenses = [...expenses];

  if (selYear !== "") {
    filteredExpenses = expenses.filter(
      (exp) => exp.createdAt.getFullYear().toString() === selYear
    );
  }

  return (
    <>
      <h1 className="text-center">My Expenses</h1>

      <div className="row mb-4">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button className="btn btn-dark" onClick={addExpenseClickHandler}>
              Add Expense
            </button>
          </div>
        </div>
        <div className="col-4">
          <ExpenseFilter
            selectedYear={selYear}
            onFilterExpense={filterExpenseHandler}
          />
        </div>
      </div>

      {isShow && (
        <ExpenseForm
          onCloseForm={closeFormHandler}
          onAddNewExpense={addNewExpenseHandler}
        />
      )}

      <div className="row">
        {filteredExpenses.map((expense) => (
          <ExpenseItem
            key={expense.id}
            id={expense.id}
            title={expense.title}
            amount={expense.amount}
            createdAt={expense.createdAt}
            onDeleteExpense={deleteExpenseHandler}
          />
        ))}
      </div>
    </>
  );
}

export default Expenses;
