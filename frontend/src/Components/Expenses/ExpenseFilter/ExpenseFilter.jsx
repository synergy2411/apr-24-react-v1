function ExpenseFilter({ onFilterExpense, selectedYear }) {
  return (
    <select
      className="form-control"
      onChange={(e) => onFilterExpense(e.target.value)}
      value={selectedYear}
    >
      <option value="">Filter by Year</option>
      <option value="2021">2021</option>
      <option value="2022">2022</option>
      <option value="2023">2023</option>
      <option value="2024">2024</option>
    </select>
  );
}

export default ExpenseFilter;
