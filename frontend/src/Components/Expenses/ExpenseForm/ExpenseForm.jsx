import { useState } from "react";
import classes from "./ExpenseForm.module.css";

function ExpenseForm(props) {
  const [enteredTitle, setEnteredTitle] = useState("");

  const [enteredAmount, setEnteredAmount] = useState(0);

  const [enteredCreatedAt, setEnteredCreatedAt] = useState("");

  const titleChangeHandler = (e) => setEnteredTitle(e.target.value);

  //   const amountChangeHandler = (e) => setEnteredAmount(e.target.value);

  const createdAtChangeHandler = (e) => setEnteredCreatedAt(e.target.value);

  const submitHandler = (e) => {
    e.preventDefault(); // Prevents to load the page again in browser

    let expense = {
      id: "e00" + Math.round(Math.random() * 100),
      title: enteredTitle,
      amount: Number(enteredAmount),
      createdAt: new Date(enteredCreatedAt),
    };

    props.onAddNewExpense(expense);
  };

  return (
    <div className={classes["backdrop"]}>
      <div className={classes["modal"]}>
        <h1 className="text-center">Add New Expense</h1>
        <form onSubmit={submitHandler}>
          {/* title */}
          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              name="title"
              id="title"
              placeholder=""
              value={enteredTitle}
              onChange={titleChangeHandler}
            />
            <label htmlFor="title">Title</label>
          </div>

          {/* amount */}
          <div className="form-floating mb-3">
            <input
              type="number"
              className="form-control"
              name="amount"
              id="amount"
              placeholder=""
              min="0.5"
              step="0.5"
              value={enteredAmount}
              onChange={(e) => setEnteredAmount(e.target.value)}
            />
            <label htmlFor="amount">Amount</label>
          </div>

          {/* createdAt */}
          <div className="form-floating mb-3">
            <input
              type="date"
              className="form-control"
              name="createdAt"
              id="createdAt"
              placeholder=""
              max="2024-03-31"
              min="2021-04-01"
              value={enteredCreatedAt}
              onChange={createdAtChangeHandler}
            />
            <label htmlFor="createdAt">Date</label>
          </div>

          {/* buttons */}
          <div className="row">
            <div className="col-6">
              <div className="d-grid">
                <button className="btn btn-primary" type="submit">
                  Add
                </button>
              </div>
            </div>
            <div className="col-6">
              <div className="d-grid">
                <button
                  className="btn btn-outline-secondary"
                  onClick={() => props.onCloseForm()}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default ExpenseForm;
