import { configureStore } from "@reduxjs/toolkit";

const initialState = {
  counter: 0,
};

// REDUCER
function rootReducer(state = initialState, action) {
  if (action.type === "INCREMENT") {
    return {
      ...state,
      counter: state.counter + 1,
    };
  }
  return state;
}

// STORE
const store = configureStore({
  reducer: rootReducer,
});

store.subscribe(() => console.log("Subscribed state: ", store.getState()));

store.dispatch({ type: "INCREMENT" });
store.dispatch({ type: "INCREMENT" });
store.dispatch({ type: "INCREMENT" });
store.dispatch({ type: "INCREMENT" });
