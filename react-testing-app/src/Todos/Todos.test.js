import { render, screen } from "@testing-library/react";
import Todos from "./Todos";

describe("Todos Compoenent", () => {
  test("renders the list of todo items", async () => {
    render(<Todos />);

    const listItems = await screen.findAllByRole("listitem");

    // expect(listItems.length).not.toBe(0);
    expect(listItems).toHaveLength(200);
  });
});
