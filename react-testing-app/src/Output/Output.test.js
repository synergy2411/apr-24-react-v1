import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Output from "./Output";

describe("Output Component", () => {
  test("renders the heading element", () => {
    render(<Output />);

    const headingEl = screen.getByText(/Output Component Loaded/, {
      exact: false,
    });

    expect(headingEl).toBeInTheDocument();
  });

  test("renders the 'Hello React' in a paragraph", () => {
    render(<Output />);
    const paragraphEl = screen.queryByText("Hello World");
    expect(paragraphEl).not.toBeNull();
  });

  test("renders 'This paragraph will display when toggle is false' in a paragraphq when toggle is false", () => {
    render(<Output />);

    const paragraphEl = screen.getByText(
      "This paragraph will display when toggle is false"
    );

    expect(paragraphEl).toBeInTheDocument();
  });

  test("renders 'Toggle value is true now' in paragraph when button is clicked", async () => {
    render(<Output />);

    const buttonEl = screen.getByRole("button");

    userEvent.click(buttonEl);

    const paragraphEl = await screen.findByText("Toggle value is true now");

    expect(paragraphEl).not.toBeNull();
  });
});
