import { useState } from "react";

function Output() {
  const [toggle, setToggle] = useState(false);

  return (
    <>
      <h1>Output Component Loaded</h1>
      <button onClick={() => setToggle(!toggle)}>Toggle</button>

      <p>Hello World</p>

      {toggle && <p>Toggle value is true now</p>}
      {!toggle && <p>This paragraph will display when toggle is false</p>}
    </>
  );
}

export default Output;
