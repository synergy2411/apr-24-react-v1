# Break timing

- Tea -> 11:00AM (15 Minutes)
- Lunch -> 01:00 (45 Minutes)
- Tea -> 03:30 (15 Minutes)

# JavaScript : Implementation of ECMA Specification

- Client side (Browser) as well as Server side (NodeJS Platform)
- Lightweight
- Single Threaded / Multi-threads
- Fast
- Synchronous as well as Asynchronous
- Weekly Typed
- Dynamic Typings
- Compiled at runtime and interpreted
- Cross Platform
- Object-oriented
- Web Development / Native App / Desktop Apps (Electron)
- Browser compatible

# JavaScript DataTypes

- Primitive Types : string, number, boolean
- Reference Types : objects, arrays, functions, date

# AST : Abstract Syntax Tree

MEAN - Full Stack; MongoDB | Express | Angular / React | Node

JSON Documents

ECMA Script - Specifications given ECMA Community
ES5 - Vanilla JavaScript; Object based
ES6 - ES2015 : Object-oriented, Classes, Arrow functions, Destructuring, Rest/Spread, map etc
ES7 - 2016
ES8 - 2017
ES9 - 2018
.
.
.

# JavaScript Libraries (specific purpose) / Framework (skeleton to create the app)

- React : Library; Rendering the UI Quickly and efficiently
- NextJS : Based on React Library; Server Side Rendering
- Angular : SPA, Form Validation, Component Architecture, Modular Approach, XHR Call, AoT Compiler etc
- jQuery : DOM Manipulation, AJAX Calls, Animations
- Vue : Framework, Virtual DOM, Syntax, "Evan You"
- Knockout : 2way data binding, MVVM Pattern
- BackboneJS : Client Side MVC
- Ember : Framework; Frequent API changes

- NodeJS : Server Side Platform
- ExpressJS, Hapi, Koa, SailsJS, KrakenJS : Server Side Web App Framework

# Atomic Design Principle (Web Designing)

- Atom : can't be further divided. eg. one button, one input field
- Molecule : combo of atoms.
  eg. SearchBar -> One input field + One Button
- Organism : combo of molecules.
  eg. NavigationBar -> BrandImage + NavLinks + SearchBar
- Template : combo of organism.
  eg. Form -> Multiple Input fields + Multiple Buttons etc
- Page : the complete page

# Thinking in React way

- If the piece of code is reusable, then make it a component

# Component Tree

One Root Component - NavigationBar - Links - Links - Links - Links - SideBar - Links - Links - Links - Links - MainSection - Articles - blockquotes - Articles - blockquotes - Articles - blockquotes

# Create React App Project

- npx create-react-app <app-name>
- npx create-react-app frontend
- cd frontend
- npm start

# To install bootstrap

- npm install bootstrap

- npm install prop-types

const [stateVar, stateChangingFn] = useState(initial_args);

# React Concepts

- Component : functional components / class based components
- Props : functional props (Lift-up the state)
- State : useState() hook

## React is used to render the UI quickly and efficiently

# Side Effect

- XHR Call
- Route transitioning
- State change

# Template Driven

- Validation logic inside template
- Form is created by developer explicitly
- ngForm, ngModel

# Reactive

- Angular validation logic
- Form is dynamically generated
  - FormArray, FormGroup, FormBuilder, FormControl

# Controlled vs Uncontrolled

- Controlled : instant feedback. eg. Registration Form
- Uncontrolled : simply fetching the value form elements.eg. Login Form

# Form Validation Library

- react-hook-form
- formik
- yup
- joi
- superstruct

# useEffect Flavors

- useEffect(cb) :

  > cb will execute at initial rendering of component
  > cb will execute after every rendering cycle

- useEffect(cb, []): (componentDidMount)

  > cb will ONLY execute at initial rendering of component
  > cb will execute only one time.

- useEffect(cb, [Dependencies]): (componentDidUpdate)

  > cb will execute at initial rendering of component
  > cb will execute whenever the dependency will change

- useEffect(cb => cleanUpFn, [Deps]): (componentWillUnmount)
  > cb will execute at initial rendering of component
  > Dependency will change
  > cleanUpFn will execute
  > cb executes after cleanUpFn
  > cleanUpFn will execute at the time of component unmounts

# useContext(CONTEXT)

- Consume the context API
- Prop-drilling

---

- useReducer() vs useState()
  > When nextState depends upon the previousState
  > When we want to maintain multiple state slices
  > When we have complex state changing logic
- useCallback()
  > works with only Callback Functions
- useMemo(() => value) -> object, array, function

  > works with all reference types including array, objects and functions

---

# Single Page App

- npx create-react-app react-spa
- cd react-spa
- npm install react-router-dom bootstrap

# JSON SERVER INSTALLATION - fake RST API Server

- npm install json-server@0.17.4 -g
- json-server --version
- Create db.json file
- json-server --watch db.json --port=3030

# POC Routes

- / => loads HomePage
- /courses => load all courses
- /courses/{courseId} => loads course details for specific course
- /courses/new => add new course
- /courses/{courseId}/edit => loads the CourseEditPage

# React Router Dom Terminologies -

- createBrowserRouter() : creates the routing configuration
- RouteProvider : makes the routing configuration available in react app.
- NavLink : creates the navigation links (top level links)
- Link : changes the URL path
- ErrorElement : defines the ErrorPage
- Outlet : defines the space to load the component in parent
- loaders : loads the data in the routes before loading the component
- useLoaderData(): to retrieve the data avialble on specific route
- useRouteLoaderData() : to retrieve the data available in the parent route

- actions : are submitted by the Form Component (given by react-router-dom)
- useNavigate() : programmatic navigation
- useSubmit() : allow us to submit form programmatically

---

# State Management

- useState() : Component Level
- useReducer() : Component Level
- Context API / useContext() :
  > Component branch level;
  > NOT highly optimized;
  > Unable to handle frequent data change
  > eg. JWT token, userPreferences etc
- Redux : State management tool
  > Frequent data change
  > Complete data at one place (Store - Centralized place for keeping data)
  > Top level component is not enough for data handling

# Redux

- redux library + redux-thunk/ redux-saga
- @reduxjs/toolkit (RTK)
- react-redux

# Redux Building Blocks

- Action : payload of information to redux store
- Reducer : pure functions to change the state
- Store : contains state slices
- Middleware : able to perform side-effects

# Basic Redux Project

- npm init -y
- npm install @reduxjs/toolkit

# React Redux App

- npx create-react-app react-redux-app
- cd react-redux-app
- npm install @reduxjs/toolkit react-redux bootstrap
- useSelector() : allows to fetch the state slice from reducers
- useDispatch() : allows to dispatch the action from the application

# Devtools Chrome Extension

- React Developers Tool
- Redux DevTools

# Class Based Component

- connect()
- mapStateToProps()
- mapDispatchToProps()

# to install Firebase SDK

- npm install firebase

  apiKey: "AIzaSyDBUIbWLPX4ZWiFFUp84z_Ss8nmxMYNszI",
  authDomain: "apr-24-react.firebaseapp.com",

# Promise

- Pending : waiting for server response
- Fulfilled : success case
- Rejected : failure case

# Testing Library -

- getBy
- queryBy
- findBy
