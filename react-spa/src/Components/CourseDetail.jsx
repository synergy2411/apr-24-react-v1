import { Form, useNavigate, useSubmit } from "react-router-dom";
import classes from "./CourseDetail.module.css";

function CourseDetail({ course }) {
  const submit = useSubmit();
  const navigate = useNavigate();

  const deleteClickHandler = () => {
    // eslint-disable-next-line no-restricted-globals
    const isConfirmed = confirm("Are you sure to delete?");
    if (!isConfirmed) {
      return;
    } else {
      submit({}, { action: `/courses/${course.id}`, method: "DELETE" });
    }
  };

  return (
    <div className={classes["backdrop"]}>
      <div className={` card ${classes["modal"]}`} style={{ width: "20rem" }}>
        <img
          className="card-img-top"
          src={course.logo}
          alt={course.title}
          style={{ width: "18rem" }}
        />
        <div className="card-body">
          <h4 className="card-title">{course.title.toUpperCase()}</h4>
          <p className="card-text">Duration : {course.duration}Hrs</p>
          <Form>
            <div className="row">
              <div className="col-6">
                <div className="d-grid">
                  <button
                    className="btn btn-outline-success"
                    onClick={() => navigate(`/courses/${course.id}/edit`)}
                  >
                    Edit
                  </button>
                </div>
              </div>
              <div className="col-6">
                <div className="d-grid">
                  <button
                    className="btn btn-light"
                    onClick={deleteClickHandler}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default CourseDetail;
