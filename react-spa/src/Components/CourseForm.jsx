import { useRef } from "react";
import { useNavigate, useSubmit } from "react-router-dom";
import classes from "./CourseDetail.module.css";

function CourseForm({ course }) {
  const navigate = useNavigate();
  const titleRef = useRef();
  const durationRef = useRef();
  const logoRef = useRef();
  const submit = useSubmit();

  const submitHandler = (event) => {
    event.preventDefault();

    let newCourse = {
      title: titleRef.current.value,
      duration: durationRef.current.value,
      logo: logoRef.current.value,
    };

    let options = { action: "/courses/new", method: "POST" };

    if (course) {
      options = { action: `/courses/${course.id}/edit`, method: "PATCH" };
    }

    submit(newCourse, options);
  };

  return (
    <div className={classes["backdrop"]}>
      <div className={classes["modal"]}>
        <h4 className="text-center"> Add New Course</h4>
        <form onSubmit={submitHandler}>
          {/* title */}
          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              name="title"
              id="title"
              placeholder=""
              defaultValue={course ? course.title : ""}
              ref={titleRef}
            />
            <label htmlFor="title">Title</label>
          </div>

          {/* duration */}
          <div className="form-floating mb-3">
            <input
              type="number"
              className="form-control"
              name="duration"
              id="duration"
              placeholder=""
              min={8}
              step={2}
              defaultValue={course ? course.duration : ""}
              ref={durationRef}
            />
            <label htmlFor="duration">Duration</label>
          </div>

          {/* logo */}
          <div className="form-floating mb-3">
            <input
              type="text"
              className="form-control"
              name="logo"
              id="logo"
              placeholder=""
              defaultValue={course ? course.logo : ""}
              ref={logoRef}
            />
            <label htmlFor="logo">Logo Address</label>
          </div>

          {/* buttons */}
          <div className="row">
            <div className="col-6">
              <div className="d-grid">
                <button className="btn btn-primary" type="submit">
                  {course ? "Edit" : "Add"}
                </button>
              </div>
            </div>
            <div className="col-6">
              <div className="d-grid">
                <button
                  type="button"
                  className="btn btn-secondary"
                  onClick={() => navigate("/courses")}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default CourseForm;
