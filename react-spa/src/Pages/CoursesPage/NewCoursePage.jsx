import { json, redirect } from "react-router-dom";
import CourseForm from "../../Components/CourseForm";

function NewCoursePage() {
  return (
    <>
      <CourseForm />
    </>
  );
}

export default NewCoursePage;

export async function action({ request }) {
  const formData = await request.formData();
  let newCourse = {
    title: formData.get("title"),
    duration: formData.get("duration"),
    logo: formData.get("logo"),
  };
  const response = await fetch("http://localhost:3030/courses", {
    method: "POST",
    body: JSON.stringify(newCourse),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (!response.ok) {
    throw json(
      {
        message: "Unable to create new Course",
      },
      { status: 404 }
    );
  }
  return redirect("/courses");
}
