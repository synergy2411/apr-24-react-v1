import { json, redirect, useRouteLoaderData } from "react-router-dom";
import CourseDetail from "../../Components/CourseDetail";

function CourseDetailPage() {
  // const course = useLoaderData();

  const course = useRouteLoaderData("course-detail-loader");

  return (
    <>
      <CourseDetail course={course} />
    </>
  );
}

export default CourseDetailPage;

export async function loader({ params }) {
  const { courseId } = params;

  const response = await fetch(`http://localhost:3030/courses/${courseId}`);
  if (!response.ok) {
    throw json(
      { message: "Unable to find course for " + courseId },
      { status: 404 }
    );
  }
  return response;
}

export async function DeleteCourseAction({ request, params }) {
  const { courseId } = params;

  const response = await fetch(`http://localhost:3030/courses/${courseId}`, {
    method: request.method,
  });

  if (!response.ok) {
    throw json(
      { message: "Unable to delete course for " + courseId },
      { status: 404 }
    );
  }

  return redirect("/courses");
}
