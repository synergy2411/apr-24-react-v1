import { json, useLoaderData, useNavigate } from "react-router-dom";
import CourseItem from "../../Components/CourseItem";

function CoursesPage() {
  const courses = useLoaderData();
  const navigate = useNavigate();

  return (
    <>
      <h1>Courses Page</h1>
      <div className="row mb-4">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button
              className="btn btn-primary"
              onClick={() => navigate("/courses/new")}
            >
              Add Course
            </button>
          </div>
        </div>
      </div>
      <div className="row">
        {courses.map((course) => (
          <CourseItem course={course} key={course.id} />
        ))}
      </div>
    </>
  );
}

export default CoursesPage;

export async function CoursesPageLoader() {
  const response = await fetch("http://localhost:3030/courses");
  if (!response.ok) {
    throw json({ message: "Unable to load the courses" }, { status: 404 });
  }
  const courses = await response.json();
  return courses;
}
