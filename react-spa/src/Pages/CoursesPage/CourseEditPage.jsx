// http://localhost:3000/courses/{cousreId}/edit

import { redirect, useRouteLoaderData } from "react-router-dom";
import CourseForm from "../../Components/CourseForm";

function CourseEditPage() {
  const course = useRouteLoaderData("course-detail-loader");

  return <CourseForm course={course} />;
}

export default CourseEditPage;

export async function action({ params, request }) {
  const { courseId } = params;

  const formData = await request.formData();

  let updatedCourse = {
    title: formData.get("title"),
    duration: formData.get("duration"),
    logo: formData.get("logo"),
  };
  const response = await fetch(`http://localhost:3030/courses/${courseId}`, {
    method: request.method,
    body: JSON.stringify(updatedCourse),
    headers: {
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    throw new Error("Unable to update the course");
  }

  return redirect("/courses");
}
