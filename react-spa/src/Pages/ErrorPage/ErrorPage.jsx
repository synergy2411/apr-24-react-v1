import { Link, useRouteError } from "react-router-dom";

function ErrorPage() {
  const error = useRouteError();

  console.log("ERROR : ", error);

  let errorMessage = "Oops! Something went wrong.";

  let errorStatus = "";

  if (error && error.data) {
    errorMessage = error.data.message;
    errorStatus = error.status;
  }
  return (
    <div className="text-center">
      <p className="display-3 mt-5">{errorMessage}</p>
      <p>Status Code : {errorStatus} </p>
      <p>
        Click <Link to="/">here</Link> to go back to the application
      </p>
    </div>
  );
}

export default ErrorPage;
