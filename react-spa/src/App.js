import { createBrowserRouter, RouterProvider } from "react-router-dom";

import CourseDetailPage, {
  loader as CourseDetailLoader,
  DeleteCourseAction,
} from "./Pages/CoursesPage/CourseDetailPage";

import CourseEditPage, {
  action as CourseEditAction,
} from "./Pages/CoursesPage/CourseEditPage";
import CoursesPage, {
  CoursesPageLoader,
} from "./Pages/CoursesPage/CoursesPage";
import NewCoursePage, {
  action as AddCourseAction,
} from "./Pages/CoursesPage/NewCoursePage";
import ErrorPage from "./Pages/ErrorPage/ErrorPage";
import HomePage from "./Pages/HomePage/HomePage";
import RootLayout from "./Pages/RootLayout/RootLayout";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,

    children: [
      {
        index: true, // http://localhost:3000
        element: <HomePage />,
      },
      {
        path: "/courses", // http://localhost:3000/courses
        element: <CoursesPage />,
        loader: CoursesPageLoader,
      },
      {
        path: "/courses/:courseId",
        loader: CourseDetailLoader,
        id: "course-detail-loader",
        action: DeleteCourseAction,
        children: [
          {
            index: true,
            element: <CourseDetailPage />,
          },
          {
            // http://localhost:3000/courses/{courseId}/edit
            path: "edit",
            element: <CourseEditPage />,
            action: CourseEditAction,
          },
        ],
      },

      {
        // http://localhost:3000/courses/new
        path: "/courses/new",
        element: <NewCoursePage />,
        action: AddCourseAction,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router}></RouterProvider>;
}

export default App;
