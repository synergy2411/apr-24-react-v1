// console.log("Starting the program");

// setTimeout(function () {
//   console.log("Timer running");
// }, 0);

// Promise.resolve("Promise API").then(console.log);

// console.log("Ending the program");

// OUTPUT
// 1. Starting
// 2. Ending
// 3. Promise API
// 4. Timer running

// Asynchronous
// - Macro Task (5s) : setTimeout, setInterval, XHR, Sockets etc
// - Micro Task (10s): Promises, queueMicroTask()

// let x = "Hello JavaScript";
// console.log(typeof x); // string

// x = 201;
// console.log(typeof x); // number

// x = true;

// x = {}
// x = []
// x = function() { }

let user = {
  name: "John",
};

let userTwo = user;

userTwo.name = "Jenny";

console.log(user.name); // "Jenny"
